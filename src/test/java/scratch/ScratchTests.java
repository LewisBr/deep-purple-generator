package scratch;

import org.apache.jena.assembler.RuleSet;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.rdf.model.*;
import org.apache.jena.reasoner.TriplePattern;
import org.apache.jena.reasoner.rulesys.ClauseEntry;
import org.apache.jena.reasoner.rulesys.Node_RuleVariable;
import org.apache.jena.reasoner.rulesys.Rule;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.util.PrintUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ScratchTests {
    @Before
    public void setup() {

    }

    @After
    public void testDown() {

    }

    @Test
    public void testRuleSet() throws Exception {
        List<ClauseEntry> head = new ArrayList<>();
        TriplePattern h1 = new TriplePattern(
                new Node_RuleVariable("?a", 1),
                NodeFactory.createURI("http://www.oliveai.com/v1/olive-rdf#hasParent"),
                new Node_RuleVariable("?b", 3)
        );
        TriplePattern h2 = new TriplePattern(
                new Node_RuleVariable("?b", 1),
                NodeFactory.createURI("http://www.oliveai.com/v1/olive-rdf#hasParent"),
                new Node_RuleVariable("?c", 3)
        );
        head.add(h1);
        head.add(h2);
        List<ClauseEntry> body = new ArrayList<>();
        TriplePattern b1 = new TriplePattern(
                new Node_RuleVariable("?a", 1),
                NodeFactory.createURI("http://www.oliveai.com/v1/olive-rdf#hasGrandparent"),
                new Node_RuleVariable("?c", 3)
        );
        body.add(b1);
        String ruleName = "ruleTest0";

        Rule newRule = new Rule(ruleName, head, body);
        RuleSet rs = RuleSet.create(Arrays.asList(newRule));
    }

    @Test
    public void testSwrlRule0() throws Exception {
        List<ClauseEntry> head = new ArrayList<>();
        TriplePattern h1 = new TriplePattern(
                new Node_RuleVariable("?a", 1),
                NodeFactory.createURI("http://www.oliveai.com/v1/olive-rdf#hasParent"),
                new Node_RuleVariable("?b", 3)
        );
        TriplePattern h2 = new TriplePattern(
                new Node_RuleVariable("?b", 1),
                NodeFactory.createURI("http://www.oliveai.com/v1/olive-rdf#hasParent"),
                new Node_RuleVariable("?c", 3)
        );
        head.add(h1);
        head.add(h2);
        List<ClauseEntry> body = new ArrayList<>();
        TriplePattern b1 = new TriplePattern(
                new Node_RuleVariable("?a", 1),
                NodeFactory.createURI("http://www.oliveai.com/v1/olive-rdf#hasGrandparent"),
                new Node_RuleVariable("?c", 3)
        );
        body.add(b1);
        String ruleName = "ruleTest0";
        Rule newRule = new Rule(ruleName, head, body);
    }

    @Test
    public void testTTLRule0() throws Exception {
        List<ClauseEntry> head = new ArrayList<>();
        TriplePattern h1 = new TriplePattern(
                new Node_RuleVariable("?a", 1),
                NodeFactory.createURI("http://www.oliveai.com/v1/olive-rdf#hasGrandparent"),
                new Node_RuleVariable("?c", 3)
        );
        head.add(h1);
        List<ClauseEntry> body = new ArrayList<>();
        TriplePattern b1 = new TriplePattern(
                NodeFactory.createVariable("?a"),
                NodeFactory.createURI("http://www.oliveai.com/v1/olive-rdf#hasParent"),
                NodeFactory.createVariable("?b")
        );
        TriplePattern b2 = new TriplePattern(
                new Node_RuleVariable("?b", 1),
                NodeFactory.createURI("http://www.oliveai.com/v1/olive-rdf#hasParent"),
                new Node_RuleVariable("?c", 3)
        );
        body.add(b1);
        body.add(b2);
        String ruleName = "ruleTest0";

        Rule newRule = new Rule(ruleName, head, body);

        System.out.println(newRule.toString());
    }

    @Test
    public void testReadSwrlFromFile() throws Exception {
        List<Rule> newRules = Rule.rulesFromURL("/Users/bryanlewis/Projects/Java/deep-purple-generator/src/test/java/scratch/bin/some.rules");
        for (Rule rule : newRules) {
            System.out.println(rule);
            TriplePattern t1 = (TriplePattern) rule.getBodyElement(0);
            System.out.println(t1.getObject());
            System.out.println(t1.getObject().getClass());
            System.out.println(t1.getPredicate());
            System.out.println(t1.getPredicate().getClass());
            System.out.println(t1.getSubject());
            System.out.println(t1.getSubject().getClass());
        }
    }

    /**
     * ###  http://www.oliveai.com/v1/olive-rdf#BuildCPA
     * :BuildCPA rdf:type owl:Class ;
     * rdfs:subClassOf :ProcessByStatus .
     */
    @Test
    public void testBuildOwlScratch() throws Exception {
        Model model = ModelFactory.createDefaultModel();
        // Prefixes
        model.setNsPrefix("", "http://www.oliveai.com/v1/olive-rdf#");
        model.setNsPrefix("owl", "http://www.w3.org/2002/07/owl#");
        model.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        model.setNsPrefix("xml", "http://www.w3.org/XML/1998/namespace");
        model.setNsPrefix("xsd", "http://www.w3.org/2001/XMLSchema#");
        model.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
        // Statements
        Statement st1 = ResourceFactory.createStatement(
                ResourceFactory.createResource("http://www.oliveai.com/v1/olive-rdf#Build"),
                ResourceFactory.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
                ResourceFactory.createResource("http://www.w3.org/2002/07/owl#Class")
        );
        model.add(st1);
        Statement st2 = ResourceFactory.createStatement(
                ResourceFactory.createResource("http://www.oliveai.com/v1/olive-rdf#Build"),
                ResourceFactory.createProperty("http://www.w3.org/2000/01/rdf-schema#subClassOf"),
                ResourceFactory.createResource("http://www.oliveai.com/v1/olive-rdf#ProcessStatus")
        );
        model.add(st2);
        // Equivalent Class
        Resource blank = ResourceFactory.createResource();

        Statement st3 = ResourceFactory.createStatement(
                ResourceFactory.createResource("http://www.oliveai.com/v1/olive-rdf#Build"),
                ResourceFactory.createProperty("http://www.w3.org/2002/07/owl##equivalentClass"),
                blank
        );
        model.add(st3);
        Statement st4 = ResourceFactory.createStatement(
                blank,
                ResourceFactory.createProperty("http://www.w3.org/2002/07/owl#hasValue"),
                ResourceFactory.createStringLiteral("BUILD")
        );
        model.add(st4);
        Statement st5 = ResourceFactory.createStatement(
                blank,
                ResourceFactory.createProperty("http://www.w3.org/2002/07/owl#onProperty"),
                ResourceFactory.createResource("http://www.oliveai.com/v1/olive-rdf#processStatusId")
        );
        model.add(st5);
        Statement st6 = ResourceFactory.createStatement(
                blank,
                ResourceFactory.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
                ResourceFactory.createResource("http://www.w3.org/2002/07/owl#restriction")
        );
        model.add(st6);

        RDFDataMgr.write(java.lang.System.out, model, org.apache.jena.riot.RDFFormat.TURTLE_PRETTY);
    }

    /**
     * # Dev applications
     * [ rdf:type :DevEnvironment, :OmegaSuite ;
     *              :urlPrefix "https://omegasuitedev.oliveai.com" ]  .
     */
    /**
     * [ a           :OmegaSuite , :DevEnvironment ;
     *   :urlPrefix  "https://omegasuitedev.oliveai.com"
     * ] .
     */
    @Test
    public void testBuildOwlBlankScratch() throws Exception {
        Model model = ModelFactory.createDefaultModel();
        // Prefixes
        model.setNsPrefix("", "http://www.oliveai.com/v1/olive-rdf#");
        model.setNsPrefix("owl", "http://www.w3.org/2002/07/owl#");
        model.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        model.setNsPrefix("xml", "http://www.w3.org/XML/1998/namespace");
        model.setNsPrefix("xsd", "http://www.w3.org/2001/XMLSchema#");
        model.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
        // Statements
        Resource blank = ResourceFactory.createResource();
        Statement st1 = ResourceFactory.createStatement(
                blank,
                ResourceFactory.createProperty("http://www.oliveai.com/v1/olive-rdf#urlPrefix"),
                ResourceFactory.createStringLiteral("https://omegasuitedev.oliveai.com")
        );
        model.add(st1);
        Statement st2 = ResourceFactory.createStatement(
                blank,
                ResourceFactory.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
                ResourceFactory.createResource("http://www.oliveai.com/v1/olive-rdf#OmegaSuite")
        );
        model.add(st2);
        Statement st3 = ResourceFactory.createStatement(
                blank,
                ResourceFactory.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
                ResourceFactory.createResource("http://www.oliveai.com/v1/olive-rdf#DevEnvironment")
        );
        model.add(st3);
        RDFDataMgr.write(java.lang.System.out, model, org.apache.jena.riot.RDFFormat.TURTLE_PRETTY);
    }

    @Test
    public void testReadOwlFromFile() throws Exception {
        Model model = RDFDataMgr.loadModel("/Users/bryanlewis/Projects/Java/deep-purple-generator/src/test/java/scratch/bin/build2.ttl");
        RDFDataMgr.write(java.lang.System.out, model, org.apache.jena.riot.RDFFormat.TURTLE_PRETTY);
        System.out.println(model.getNsPrefixMap());
//        for (Map.Entry<String, String> entries : model.getNsPrefixMap().entrySet()) {
//            System.out.println(entries.getKey());
//            System.out.println(entries.getValue());
//        }
//        Graph graph = model.getGraph();
//        System.out.println(graph.getPrefixMapping().numPrefixes());
//        ExtendedIterator<Triple> ei = graph.find();
//        for(Object o : ei.toList()){
//            System.out.println(o.toString());
//        }
//        ei.forEachRemaining((t) -> {
//            System.out.println(t.toString());
//        });
//        System.out.println(graph.size());
        //        System.out.println(model.toString());
    }
    @Test
    public void testReadSWRLFromFile() throws Exception {
        List<Rule> rules = Rule.rulesFromURL("/Users/bryanlewis/Projects/Java/deep-purple-generator/src/test/java/scratch/bin/some.rules");
        PrintUtil.registerPrefix("olive","http://www.oliveai.com/v1/olive-rdf#");
        for(Rule rule : rules){
            System.out.println(PrintUtil.print(rule));
        }
    }
}
