(ns dao.OliveManagerDAO_test
  (:use clojure.test)
  (:use dao.OliveManagerDAO)
  (:require [clojure.java.jdbc :as jdbc])
  (:import (java.sql SQLException)))

(deftest OliveManagerDAOConnectionTests
  (is
    (= (.first (jdbc/query mysql-db
                           ["SELECT true"]
                           {:row-fn :true}
                           )
               )
       1
       )
    "Initial Connection Query Test"
    )
  (is
    (thrown? SQLException (jdbc/query {:dbtype "mysql" :host "127.0.0.1" :port 3306 :dbname "NOSCHEMA" :user "NOUSER" :password "NOPASS"}
                                      ["SELECT true"]
                                      )
             )
    "Initial Connection Query Test"
    )
  )