(ns exception.DeepPurpleException_test
  (:use clojure.test)
  (:import (exception DeepPurpleException DeepPurpleExceptionType)))

(deftest DeepPurpleExceptionFunctionalityTests
  ;; Make sure DeepPurpleException is throwable, and accepts correct super constructor
  (is (thrown? DeepPurpleException (throw (DeepPurpleException.
                                                     (DeepPurpleExceptionType/GENERIC_ERROR))
                                                   )
               )
      )
  "DeepPurpleException Throw Test"
  ;; Make sure DeepPurpleException super constructor appropriately maps the DeepPurpleExceptionType
  (is (=
        (.getType (DeepPurpleException.
                    (DeepPurpleExceptionType/GENERIC_ERROR)))
        )
      (DeepPurpleExceptionType/GENERIC_ERROR)
      )
  "DeepPurpleException GENERIC_ERROR Check"
  ;; Make sure Overloaded Exception.getMessage correctly maps to DeepPurpleException.getDescription
  (is (=
        (.getMessage (DeepPurpleException.
                    (DeepPurpleExceptionType/GENERIC_ERROR)))
        )
      (.getDescription DeepPurpleExceptionType/GENERIC_ERROR)
      )
  "DeepPurpleException->Exception getMessage <-> getDescription Test"
  )