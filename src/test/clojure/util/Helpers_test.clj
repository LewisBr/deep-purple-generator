(ns util.Helpers_test
  (:use clojure.test)
  (:use util.Helpers)
  (:require [util.Constants :as Constants])
  (:import (java.io FileNotFoundException)
           (util Constants)))

(deftest getYamlConfigurationForResourceTests
  (is (thrown? FileNotFoundException (getYamlConfigurationForResource "" []))
      "Throw FileNotFoundException on Invalid Resource File."
      )
  (is (not (empty? (getYamlConfigurationForResource "ApplicationConfiguration.yml" [:target :ontologies])))
      "Retrieve Entry on a nested path."
      )
  (is (not (empty? (getYamlConfigurationForResource "ApplicationConfiguration.yml" [])))
      "Retrieve entire map from yaml."
      )
  (is (nil? (getYamlConfigurationForResource "ApplicationConfiguration.yml" [:INVALIDKEY]))
      "Retrieve an empty key based on an invalid KeyWord."
      )
  )

(deftest generateURIFromPrefixAndValueTests

  )

(deftest stripLastCharacterFromStringTests
  (is (empty? (stripLastCharacterFromString nil))           ;; Strip last character from nil --> ""
      "Strip Last Character from Nil")
  (is (empty? (stripLastCharacterFromString "A"))
      "Strip Last Character from Single Character")
  (is (thrown? IllegalArgumentException (stripLastCharacterFromString 1))
      "Strip Last Character from Non-String")
  (is (= (stripLastCharacterFromString "ABCDEFG")
         "ABCDEF")
      "Strip Last Character from String")
  (is (= (stripLastCharacterFromString (get Constants/defaultNSPrefixMap "owl")) ;; http://www.w3.org/2002/07/owl#
         "http://www.w3.org/2002/07/owl"
         )
      "Strip Last Character from URI")
  )

(deftest formattedBaseStringTests

  )

(deftest makeDirectoryPathForFileNameTests

  )

(deftest getGeneratedMetadataCommentTests

  )

(deftest replicateStaticFilesToTargetTests

  )

(run-tests `util.Helpers_test)