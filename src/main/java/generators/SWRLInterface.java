package generators;

import org.apache.jena.reasoner.rulesys.Rule;

import java.util.List;
import java.util.Map;

public interface SWRLInterface {
    List<Rule> generateRuleSet(Map<String,?> args);
    void generateAndWriteRuleSet(Map<String,?> args);
}
