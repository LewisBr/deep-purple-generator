package generators;

import org.apache.jena.rdf.model.Model;

import java.util.Map;

public interface TurtleInterface {
    Model generateModel(Map<String,?> args);
    void generateAndWriteModel(Map<String,?> args);
}
