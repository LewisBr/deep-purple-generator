package exception;

public class DeepPurplePipelineException extends Exception {
    final DeepPurplePipelineExceptionType type;

    public DeepPurplePipelineException(final DeepPurplePipelineExceptionType type) {
        super(type.description);
        this.type = type;
    }

    public int getErrorCode() {
        return type.errorCode;
    }

    public DeepPurplePipelineExceptionType getType() {
        return type;
    }

    @Override
    public String getMessage() {
        return type.description;
    }
}
