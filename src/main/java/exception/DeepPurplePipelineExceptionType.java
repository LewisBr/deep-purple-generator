package exception;

import org.apache.commons.lang3.StringUtils;

import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static java.util.Collections.unmodifiableSortedMap;

public enum DeepPurplePipelineExceptionType {
    //@formatter:off
    GENERIC_ERROR(0, "Generic Error. [3]");
    //@formatter:on

    final int errorCode;
    final String description;
    private static final Supplier<SortedMap<String, DeepPurplePipelineExceptionType>> sortedMapSupplier = () -> new TreeMap<>((s1, s2) -> StringUtils.trimToEmpty(s1).compareToIgnoreCase(StringUtils.trimToEmpty(s2)));
    static final SortedMap<Integer, DeepPurplePipelineExceptionType> typeByErrorCode = unmodifiableSortedMap(Stream.of(values()).collect(TreeMap::new, (m, v) -> m.put(v.errorCode, v), TreeMap::putAll));
    static final SortedMap<String, DeepPurplePipelineExceptionType> typeByName = unmodifiableSortedMap(Stream.of(values()).collect(sortedMapSupplier, (m, v) -> m.put(v.description, v), SortedMap::putAll));

    DeepPurplePipelineExceptionType(final int errorCode, final String description) {
        this.errorCode = errorCode;
        this.description = description;
    }

    public static Optional<DeepPurplePipelineExceptionType> fromErrorCode(final int errorCode) {
        return Optional.ofNullable(typeByErrorCode.get(errorCode));
    }

    public static Optional<DeepPurplePipelineExceptionType> fromName(final String name) {
        return Optional.ofNullable(typeByName.get(name));
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return String.format("%s(%s)", super.toString(), description);
    }
}
