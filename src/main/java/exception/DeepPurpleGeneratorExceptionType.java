package exception;

import org.apache.commons.lang3.StringUtils;

import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static java.util.Collections.unmodifiableSortedMap;

public enum DeepPurpleGeneratorExceptionType {
    //@formatter:off
    GENERIC_ERROR(0, "Generic Error. [2]");
    //@formatter:on

    final int errorCode;
    final String description;
    private static final Supplier<SortedMap<String, DeepPurpleGeneratorExceptionType>> sortedMapSupplier = () -> new TreeMap<>((s1, s2) -> StringUtils.trimToEmpty(s1).compareToIgnoreCase(StringUtils.trimToEmpty(s2)));
    static final SortedMap<Integer, DeepPurpleGeneratorExceptionType> typeByErrorCode = unmodifiableSortedMap(Stream.of(values()).collect(TreeMap::new, (m, v) -> m.put(v.errorCode, v), TreeMap::putAll));
    static final SortedMap<String, DeepPurpleGeneratorExceptionType> typeByName = unmodifiableSortedMap(Stream.of(values()).collect(sortedMapSupplier, (m, v) -> m.put(v.description, v), SortedMap::putAll));

    DeepPurpleGeneratorExceptionType(final int errorCode, final String description) {
        this.errorCode = errorCode;
        this.description = description;
    }

    public static Optional<DeepPurpleGeneratorExceptionType> fromErrorCode(final int errorCode) {
        return Optional.ofNullable(typeByErrorCode.get(errorCode));
    }

    public static Optional<DeepPurpleGeneratorExceptionType> fromName(final String name) {
        return Optional.ofNullable(typeByName.get(name));
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return String.format("%s(%s)", super.toString(), description);
    }
}
