package exception;

public class DeepPurpleException extends Exception {
    final DeepPurpleExceptionType type;

    public DeepPurpleException(final DeepPurpleExceptionType type) {
        super(type.description);
        this.type = type;
    }

    public int getErrorCode() {
        return type.errorCode;
    }

    public DeepPurpleExceptionType getType() {
        return type;
    }

    @Override
    public String getMessage() {
        return type.description;
    }
}
