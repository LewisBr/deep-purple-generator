package exception;

public class DeepPurpleGeneratorException extends Exception {
    final DeepPurpleGeneratorExceptionType type;

    public DeepPurpleGeneratorException(final DeepPurpleGeneratorExceptionType type) {
        super(type.description);
        this.type = type;
    }

    public int getErrorCode() {
        return type.errorCode;
    }

    public DeepPurpleGeneratorExceptionType getType() {
        return type;
    }

    @Override
    public String getMessage() {
        return type.description;
    }
}
