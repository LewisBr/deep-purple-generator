(ns util.ModelUtil
  (:require [util.Constants :as Constants]
            [util.Helpers :as Helpers])
  (:import (org.apache.jena.riot RDFDataMgr RDFFormat)
           (org.apache.jena.rdf.model ModelFactory ResourceFactory)
           (java.io FileOutputStream File))
  (:gen-class)
  )

(defn getStandardModel [baseExtension & [additionalPrefixes]]
  ;; Create Model
  (def model (ModelFactory/createDefaultModel))
  ;; Add Additional Entries
  (doseq [additionalPrefix additionalPrefixes]
    (. model setNsPrefix (.key additionalPrefix) (val additionalPrefix))
    )
  ;; Add Default Entries
  (doseq [defaultPrefix Constants/defaultNSPrefixMap]
    (. model setNsPrefix (.key defaultPrefix) (val defaultPrefix))
    )
  ;; Add Entry for Extension Base
  (. model setNsPrefix "" (Helpers/formattedBaseString baseExtension))
  )

(defn prettyPrintModel [model]
  (RDFDataMgr/write
    java.lang.System/out
    model
    RDFFormat/TURTLE_PRETTY)
  )

(defn- addMetaDataToModel [model generatorName & [optionalParams]]
  ;; Add entry for : rdf:type owl:Ontology
  (.add model (ResourceFactory/createStatement
                (ResourceFactory/createResource (Helpers/stripLastCharacterFromString (.getNsPrefixURI model "")))
                (ResourceFactory/createProperty (Helpers/generateURIFromPrefixAndValue "rdf" "type"))
                (ResourceFactory/createResource (Helpers/generateURIFromPrefixAndValue "owl" "Ontology"))
                )
        )

  ;; Add entry for : rdfs:comment #{Metadata}
  (.add model (ResourceFactory/createStatement
                (ResourceFactory/createResource (Helpers/stripLastCharacterFromString (.getNsPrefixURI model "")))
                (ResourceFactory/createProperty (Helpers/generateURIFromPrefixAndValue "rdfs" "comment"))
                (ResourceFactory/createStringLiteral (Helpers/getGeneratedMetadataComment generatorName optionalParams))
                )
        )

  )

(defn writeModelToFile [model generatorName & [optionalParams]]
  ;; Add metadata to Model
  (addMetaDataToModel model generatorName optionalParams)

  (def file-path (str (Helpers/getYamlConfigurationForResource "ApplicationConfiguration.yml"
                                                               [:target :ontologies])
                       "ttl/" generatorName ".ttl"))
  (Helpers/makeDirectoryPathForFileName file-path)
  (let [file-output-stream (new FileOutputStream
                                (new File
                                     file-path))]
    (try
      (do
        (org.apache.jena.riot.RDFDataMgr/write
          file-output-stream
          model
          org.apache.jena.riot.RDFFormat/TURTLE_PRETTY))
      )
    )
  )