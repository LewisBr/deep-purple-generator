(ns util.SWRLUtil
  (:require [util.Constants :as Constants]
            [util.Helpers :as Helpers])
  (:import (org.apache.jena.util PrintUtil)
           (java.io FileWriter)
           (org.apache.jena.reasoner.rulesys.impl RuleStore))
  (:gen-class)
  )

(defn- writePrefixes [writer & [optionalPrefixMap]]
  (doseq [prefix Constants/defaultNSPrefixMap]
    (.write writer (str "@Prefix " (.key prefix) ":\t<" (val prefix) "> .\n"))
    )
  (doseq [prefix optionalPrefixMap]
    (.write writer (str "@Prefix " (.key prefix) ":\t<" (val prefix) "> .\n"))
    )
  (.write writer "\n")
  )

(defn- writeMetadata [writer fileName optionalParams]
    (.write writer (str "\n### " (Helpers/getGeneratedMetadataComment fileName optionalParams)))
  )

(defn writeRuleStoreToFile [rules fileName & [additionalPrefixes optionalParams]]
  ;; Init PrintUtils Prefixes
  ;; Todo This may cause some weird issues later since we're registering it to the instance, if we multithread or another odd case it might cause some weird conflicts
  (PrintUtil/registerPrefixMap Constants/defaultNSPrefixMap)
  ;; Generate File Stream
  (def file-path (str (Helpers/getYamlConfigurationForResource "ApplicationConfiguration.yml"
                                                               [:target :ontologies])
                      "swrl/" fileName ".swrl"))
  (Helpers/makeDirectoryPathForFileName file-path)
  (let [writer (new FileWriter file-path)]
    (try
      (do
        (writePrefixes writer additionalPrefixes)
        (doseq [rule rules]
          (.write writer (str (.toString rule) "\n"))
          )
        (writeMetadata writer fileName optionalParams)
        (.close writer)
        )
      )
    )
  )



