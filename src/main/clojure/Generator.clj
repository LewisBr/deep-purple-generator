(ns Generator
  (:require [util.Helpers :as Helpers]
            [taoensso.timbre
             :refer [info]])
  (:gen-class)
  )


(def propertyMap (Helpers/getYamlConfigurationForResource "GeneratorRunConfiguration.yml"
                                                  [:generators])
  )

(defn- runGenerators []
  ;; Copy Static Files, Also creates a base layout of :target/ontologies/[ttl/swrl]/
  (Helpers/replicateStaticFilesToTarget)

  ;; Generate Turtle Rules
  (doseq [interfaceModel (Helpers/getClassesThatContainSuper generators.TurtleInterface)]
    (info (str "Running Model: " + interfaceModel))
    (.generateAndWriteModel (Helpers/reflectNewInstanceFromClass interfaceModel) (get-in propertyMap [:ttl (keyword (.getSimpleName interfaceModel))]))
    )

  ;; Generate SWRL Rules
  (doseq [interfaceModel (Helpers/getClassesThatContainSuper generators.SWRLInterface)]
    (info (str "Running RuleSet: " + interfaceModel))
    (.generateAndWriteRuleSet (Helpers/reflectNewInstanceFromClass interfaceModel) (get-in propertyMap [:swrl (keyword (.getSimpleName interfaceModel))]))
    )
  )

(defn -main [& args]
  (info "Beginning DeepPurple-Generator")
  ;; Load all generators into the clojure ClassLoader
  (Helpers/loadGeneratorsToClassLoader)
  (runGenerators)
  (info "Completed DeepPurple-Generator")
  )