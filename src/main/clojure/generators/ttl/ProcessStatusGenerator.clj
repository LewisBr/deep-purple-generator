(ns generators.ttl.ProcessStatusGenerator
  (:require [dao.OliveManagerDAO :as OliveDAO]
            [util.ModelUtil :as ModelUtil]
            [util.Helpers :as Helpers]
            [taoensso.timbre
             :refer [info]])
  (:import (org.apache.jena.rdf.model ResourceFactory))
  (:gen-class
    :implements [generators.TurtleInterface])
  )
(def GENERATOR_NAME "ProcessStatus")
(def DEFAULT_CONFIGURATION {
                            :keyOne "VALUE3"
                            :keyTwo "VALUE4"
                            }
  )

(defn- getProcessStatuses []
  (info (str "Starting #getProcessStatuses"))
  (OliveDAO/getDistinctColumnValuesByOccurrenceLimit "process" "status" :limit 10)
)

(defn -generateModel
  ([] (-generateModel nil DEFAULT_CONFIGURATION))
  ([configuration] (-generateModel nil configuration))
  ([this args]
  (info (str "Starting " GENERATOR_NAME "#generateModel with Arguments:" args))
  ;; GET AVAILABLE PROCESS STATUSES
  (def processStatuses (getProcessStatuses))

  ;; Generate Model
  (def model (ModelUtil/getStandardModel GENERATOR_NAME))
  (def prefixMap (.getNsPrefixMap model))
  (doseq [processStatus processStatuses]
    (info (str "Processing Status: " processStatus))
    ;; Add Base Element
    (.add model (ResourceFactory/createStatement
                  (ResourceFactory/createResource (Helpers/generateURIFromPrefixAndValue "" processStatus prefixMap))
                  (ResourceFactory/createProperty (Helpers/generateURIFromPrefixAndValue "rdf" "type" prefixMap))
                  (ResourceFactory/createResource (Helpers/generateURIFromPrefixAndValue "owl" "Class" prefixMap)))
          )
    ;; Add Subclass Declaration
    (.add model (ResourceFactory/createStatement
                  (ResourceFactory/createResource (Helpers/generateURIFromPrefixAndValue "" processStatus prefixMap))
                  (ResourceFactory/createProperty (Helpers/generateURIFromPrefixAndValue "rdfs" "subClassOf" prefixMap))
                  (ResourceFactory/createResource (Helpers/generateURIFromPrefixAndValue "olive" "ProcessStatus" prefixMap)))
          )
    ;; Add Equivalent Class (Blank Node) Declarations
    (def blankNode (ResourceFactory/createResource))
    (.add model (ResourceFactory/createStatement
                  (ResourceFactory/createResource (Helpers/generateURIFromPrefixAndValue "" processStatus prefixMap))
                  (ResourceFactory/createProperty (Helpers/generateURIFromPrefixAndValue "owl" "equivalentClass" prefixMap))
                  blankNode)
          )
    ;; Add Blank Node Value Check
    (.add model (ResourceFactory/createStatement
                  blankNode
                  (ResourceFactory/createProperty (Helpers/generateURIFromPrefixAndValue "owl" "hasValue" prefixMap))
                  (ResourceFactory/createStringLiteral processStatus)
                  )
          )
    ;; Add onProperty Check for BlankNode
    (.add model (ResourceFactory/createStatement
                  blankNode
                  (ResourceFactory/createProperty (Helpers/generateURIFromPrefixAndValue "owl" "onProperty" prefixMap))
                  (ResourceFactory/createResource (Helpers/generateURIFromPrefixAndValue "olive" "processStatusId" prefixMap))
                  )
          )
    ;; Add onProperty Restriction for BlankNode
    (.add model (ResourceFactory/createStatement
                  blankNode
                  (ResourceFactory/createProperty (Helpers/generateURIFromPrefixAndValue "rdf" "type" prefixMap))
                  (ResourceFactory/createResource (Helpers/generateURIFromPrefixAndValue "owl" "restriction" prefixMap))
                  )
          )
  )
  model)
  )

(defn -generateAndWriteModel [this args]
  (def model (-generateModel this args))
  (ModelUtil/writeModelToFile model GENERATOR_NAME)
  )