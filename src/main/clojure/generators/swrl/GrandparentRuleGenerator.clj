(ns generators.swrl.GrandparentRuleGenerator
  (:require [util.SWRLUtil :as SWRLUtil]
            [taoensso.timbre
             :refer [info]])
  (:import (org.apache.jena.reasoner.rulesys Rule)
           (org.apache.jena.reasoner TriplePattern)
           (org.apache.jena.graph NodeFactory))
  (:gen-class
    :implements [generators.SWRLInterface])
  )
(def RULE_NAME "GrandParentRule")

(defn -generateRuleSet [this args]
  (info (str "Starting " RULE_NAME "#getGrandParentSWRLRule with Arguments:" args))
  (def head (list (new TriplePattern
                   (NodeFactory/createVariable "a")
                   (NodeFactory/createURI "http://www.oliveai.com/v1/olive-rdf#hasParent")
                   (NodeFactory/createVariable "b")
                   )
             (new TriplePattern
                  (NodeFactory/createVariable "b")
                  (NodeFactory/createURI "http://www.oliveai.com/v1/olive-rdf#hasParent")
                  (NodeFactory/createVariable "c")

                  )
                  ))

  (def body (list (new TriplePattern
                 (NodeFactory/createVariable "a")
                 (NodeFactory/createURI "http://www.oliveai.com/v1/olive-rdf#hasGrandParent")
                 (NodeFactory/createVariable "c")
                 )))

  (list (new Rule
                    RULE_NAME
                    body
                    head))
  )

(defn -generateAndWriteRuleSet [this args]
  (def rules (-generateRuleSet this args))
  (SWRLUtil/writeRuleStoreToFile rules RULE_NAME)
  )