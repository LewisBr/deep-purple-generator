(ns dao.OliveManagerDAO
  (:require [clojure.java.jdbc :as jdbc]
            [util.Helpers :as Helpers])
  )
(gen-class
  :name OliveManagerDAO)

(def mysql-db (Helpers/getYamlConfigurationForResource "connectors/DBConnectors.yml"
                                                       [:mySql :olive-manager-api]))

(defn- queryForDistinctColumnValuesByOccurrenceLimit [table column limit]
  (jdbc/query mysql-db
              [(str "SELECT " column ", count(*) AS count FROM " table " GROUP BY " column " HAVING count >= " limit)]
              {:row-fn :status})
  )

(defn getDistinctColumnValuesByOccurrenceLimit [table column &{:keys [limit] :or {limit 0}}]
  (queryForDistinctColumnValuesByOccurrenceLimit table column limit)
)