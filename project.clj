(def SPRING-BOOT-VERSION "2.1.4.RELEASE")

;; build: lein clean && lein spring-boot-jar
;; run: java -jar target/boot-clj-spring-demo-1.0.jar

(defproject DeepPurple-Generator "1.0"
  :description "Generator framework to generate OWL and SWRL Rules programmatically"
  :dependencies [
                 [org.clojure/core.async "1.2.603"]
                 [org.clojure/clojure "1.10.0"]
                 [org.clojure/tools.namespace "1.0.0"]
                 ;; Lein
                 [lein-ancient "0.6.15"]
                 ;; Jena Dependencies
                 [org.apache.jena/jena-tdb2 "3.14.0"] ;; i think this one entails all the other jena deps below
                 [org.apache.jena/jena-arq "3.14.0"]
                 [org.apache.jena/jena-base "3.14.0"]
                 [org.apache.jena/jena-core "3.14.0"]
                 ;; Commons
                 [commons-io/commons-io "2.7"]
                 ;; Yaml Parser
                 [clj-commons/clj-yaml "0.7.0"]
                 ;; DBMS
                 [org.clojure/java.jdbc "0.7.10"]
                 [mysql/mysql-connector-java "8.0.21"]
                 ;; Logging
                 [com.taoensso/timbre "4.10.0"]
                 [org.slf4j/slf4j-simple "1.7.26"]          ;; Clearing some logging warnings from dependencies
                 ]
  :main Generator
  :aot :all
  :test-paths ["src/test/clojure" "src/test/java"]
  :source-paths ["src/main/clojure"]
  :resource-paths ["src/main/resources/default"]
  :java-source-paths ["src/main/java"]
  :profiles {
             :dp-dev {:resource-paths ["src/main/resources/dev"
                                    "src/main/resources/default"]}
             }
  :aliases {
            "generator" ["do" "clean," "run" "-m" "Generator"]
            "pipeline" ["run" "-m" "Pipeline"]
            "deep-purple" ["do" "generator," "pipeline"]
            "deep-purple:dev" ["do" "with-profile" "dp-dev" "deep-purple"]
            }
  )